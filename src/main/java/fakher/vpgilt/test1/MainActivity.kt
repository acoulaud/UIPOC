package fakher.vpgilt.test1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.view.View


class MainActivity : AppCompatActivity() , View.OnClickListener{
    override fun onClick(p0: View?) {

        when(p0?.id){
            R.id.floatingB -> page2()
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<FloatingActionButton>(R.id.floatingB).setOnClickListener(this)


    }


    fun page2() {


        val intent = Intent(this, ecran2::class.java)

        startActivity(intent);
    }
}
