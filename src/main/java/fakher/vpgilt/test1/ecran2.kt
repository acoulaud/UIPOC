package fakher.vpgilt.test1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_ecran2.*
import android.widget.*
import com.github.gcacace.signaturepad.views.SignaturePad
import fakher.vpgilt.test1.R.id.signaturePad






class ecran2 : AppCompatActivity() {

    var sl = arrayOf("Très Satisfait", "Satisfait","Peu Statisfait","Insatisfait")


    var signaturePad: SignaturePad? = null




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ecran2)

        setUpSpinner()


        signaturePad?.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onStartSigning() {

            }

            override fun onSigned() {

            }

            override fun onClear() {

            }
        })

    }


   fun RadioButtonClicked(view :View)
   {

       var checked = (view as RadioButton).isChecked;

       // Check which radio button was clicked
       when (view.getId()) {
           R.id.radioButton6 -> {
               if (checked)
               // Pirates are the best
               {
                   Toast.makeText(this,"radio button 6",Toast.LENGTH_LONG).show()
               }

           }
           R.id.radioButton7 ->{

               if (checked)

               {
                   Toast.makeText(this,"radio button 7",Toast.LENGTH_LONG).show()
               }
           }
           R.id.radioButton8 ->{

               if (checked)

               {
                   Toast.makeText(this,"radio button 8",Toast.LENGTH_LONG).show()
               }
           }
           R.id.radioButton9 ->{

               if (checked)

               {
                   Toast.makeText(this,"radio button 9",Toast.LENGTH_LONG).show()
               }
           }

       }


   }

   fun setUpSpinner(){
       // Set Adapter to Spinner
       // Initializing an ArrayAdapter
       val adapter = ArrayAdapter(
               this, // Context
               android.R.layout.simple_spinner_item, // Layout
               sl // Array
       )
       // Set the drop down view resource
       adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)

       // Finally, data bind the spinner object with dapter
       spinner.adapter = adapter;

       //item selected listener for spinner
       spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
           override fun onNothingSelected(p0: AdapterView<*>?) {
               TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
           }

           override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {

               et1.setText(sl[p2])

           }

       }
   }

}
